package de.unirostock.database;


import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexManager;
import org.neo4j.helpers.collection.MapUtil;
import org.neo4j.kernel.EmbeddedGraphDatabase;

import de.unirostock.configuration.Config;

public class Manager {

	private static Manager INSTANCE = null;
	private GraphDatabaseService  graphDb = null;


	private Index<Node> nodeFullTextIndex = null;
	private Index<Node> annotationExactIndex = null;
	private Index<Node> annotationFullTextIndex = null;
	private Index<Relationship> relationshipIndex = null;
	private Index<Node> publicationFullTextIndex = null;
	private Index<Node> personExactIndex = null;
	

	private Manager() {
		graphDb = new EmbeddedGraphDatabase(Config.dbPath);
		registerShutdownHook(graphDb);
		
		//Index<Node> index = graphDb.index().forNodes( "my-case-insensitive-index", stringMap( "analyzer", LowerCaseKeywordAnalyzer.class.getName() ) );
		
		nodeFullTextIndex = graphDb.index().forNodes("nodeFullTextIndex", MapUtil.stringMap(IndexManager.PROVIDER, "lucene", "type", "fulltext"));
		relationshipIndex = graphDb.index().forRelationships("relationshipIndex");
		annotationFullTextIndex = graphDb.index().forNodes("annotationFullTextIndex", MapUtil.stringMap( IndexManager.PROVIDER, "lucene", "type", "fulltext" ));
		annotationExactIndex = graphDb.index().forNodes("annotationExactIndex", MapUtil.stringMap( IndexManager.PROVIDER, "lucene", "type", "exact" ));	
		publicationFullTextIndex = graphDb.index().forNodes("publicationFullTextIndex", MapUtil.stringMap( IndexManager.PROVIDER, "lucene", "type", "fulltext" ));
		personExactIndex = graphDb.index().forNodes("personExactIndex", MapUtil.stringMap( IndexManager.PROVIDER, "lucene", "type", "exact" ));
	}
	

	public static synchronized Manager instance() {
		if (INSTANCE == null) {
			INSTANCE = new Manager();
		}
		return INSTANCE;
	}
	
	public GraphDatabaseService  getDatabase(){
		return Manager.INSTANCE.graphDb;
	}
	

	public Index<Node> getNodeFullTextIndex() {
		return nodeFullTextIndex;
	}


	public Index<Node> getAnnotationExactIndex() {
		return annotationExactIndex;
	}


	public Index<Node> getAnnotationFullTextIndex() {
		return annotationFullTextIndex;
	}


	public Index<Relationship> getRelationshipIndex() {
		return relationshipIndex;
	}


	public Index<Node> getPublicationFullTextIndex() {
		return publicationFullTextIndex;
	}


	public Index<Node> getPersonExactIndex() {
		return personExactIndex;
	}


	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

	private static void registerShutdownHook(final GraphDatabaseService graphDb) {
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits (even if you "Ctrl-C" the
		// running example before it's completed)
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDb.shutdown();
			}
		});
	}

}
