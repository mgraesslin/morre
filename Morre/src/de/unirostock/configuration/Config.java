package de.unirostock.configuration;

public class Config {

	public static final String[] nodeIndexFields = { Property.SBML.NAME,
			Property.General.ID, Property.SBML.REACTION,
			Property.SBML.SPECIES, Property.SBML.COMPARTMENT, 
			Property.SBML.NON_RDF, Property.General.CREATOR, 
			Property.General.AUTHOR};
	public static final String[] publicationFields = {Property.Publication.ABSTRACT,
			Property.Publication.AFFILIATION, Property.Publication.AUTHOR, 
			Property.Publication.JOURNAL, Property.Publication.TITLE, 
			Property.Publication.YEAR};
	// public static final String[] annotationIndexFields;
	public static String dbPath;
	public static String submitter;

}
