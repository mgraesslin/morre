package de.unirostock.annotation;

import org.apache.commons.lang.StringUtils;
import org.htmlparser.parserapplications.StringExtractor;
import org.htmlparser.util.ParserException;

import uk.ac.ebi.miriam.lib.MiriamLink;

public class AnnotationUtil {
	//static FileWriter preLog;
	//static long i = 0;

	public static String getURIFullText(String uri) {

		String[] res = {};
		try {
			MiriamLink link = new MiriamLink();
			link.setAddress("http://www.ebi.ac.uk/miriamws/main/MiriamWebServices");
			res = link.getLocations(uri);
		} catch (Exception e1) {
			e1.printStackTrace();
			return "";
		}
		StringBuffer sb = new StringBuffer();
		sb.append(" ");
		if (res == null) {
//			try {
//				BufferedWriter bw = new BufferedWriter(new FileWriter("d:/temp/uriErrors.log"));
//				bw.write(Long.toString(i) + "\t" + uri);
//				bw.newLine();
//				bw.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			i++;
			System.out.println("MiriamLink returned null as locations for "
					+ uri);
			return "";
		}
		for (String location : res) {
			try {
				if (StringUtils.isEmpty(location)){continue;}
				StringExtractor se = new StringExtractor(location);
				sb.append(se.extractStrings(false));
				sb.append(" ");
			} catch (ParserException e) {
				e.printStackTrace();
				continue;
			} catch (Exception e1) {
				e1.printStackTrace();
				continue;
			}
		}

		return sb.toString();
	}

}
