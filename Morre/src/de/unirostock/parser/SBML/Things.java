package de.unirostock.parser.SBML;

import java.util.LinkedList;
import java.util.List;

public class Things {
	
	public class Person{
		private String firstName;
		private String lastName;
		private String email;
		
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		
		public Person(String fistName, String lastName, String email){
			this.email = email;
			this.firstName = fistName;
			this.lastName = lastName;
		}
		
	}
	
	public class Publication{
		
		private String titel;
		private String jounral;
		private String affiliation;
		private String synopsis;
		private String year;
		private List<Person> authors = new LinkedList<Person>();
		
		public void addAuthor(Person author){
			authors.add(author);
		}
		
		public Publication(String title, String jounral, String synopsis, String affiliation, String year, List<Person> authors){
			this.affiliation = affiliation;
			this.jounral = jounral;
			this.synopsis = synopsis;
			this.titel = title;
			this.year = year;			
			if (authors!=null) this.authors = authors;
		}

		public String getTitel() {
			return titel;
		}

		public void setTitel(String titel) {
			this.titel = titel;
		}

		public String getJounral() {
			return jounral;
		}

		public void setJounral(String jounral) {
			this.jounral = jounral;
		}

		public String getAffiliation() {
			return affiliation;
		}

		public void setAffiliation(String affiliation) {
			this.affiliation = affiliation;
		}

		public String getSynopsis() {
			return synopsis;
		}

		public void setSynopsis(String synopsis) {
			this.synopsis = synopsis;
		}

		public String getYear() {
			return year;
		}

		public void setYear(String year) {
			this.year = year;
		}

		public List<Person> getAuthors() {
			return authors;
		}

		public void setAuthors(List<Person> authors) {
			this.authors = authors;
		}
		
	}

}
