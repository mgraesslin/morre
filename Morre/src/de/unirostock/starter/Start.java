package de.unirostock.starter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import javax.xml.stream.XMLStreamException;

import de.unirostock.configuration.Config;
import de.unirostock.database.Manager;
import de.unirostock.parser.SBML.SBMLExtractor;

public class Start {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String modelDir = null;
		//parse arguments
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-dbPath")) { 
				Config.dbPath = args[++i];
			}
//			if (args[i].equals("-submitter")) { 
//				Config.submitter = args[++i];
//			}
			if (args[i].equals("-directory")) { 
				modelDir = args[++i];
			}
			
		}
		
		//create neo4j database
		long start = System.currentTimeMillis();
		System.out.println("Started at: " + new Date());
		System.out.print("Getting manager...");
		Manager.instance();
		System.out.println("done");
		//parse and store a model
		File directory = new File(modelDir);
		File[] files = directory.listFiles();
		
		for (File file : files) {
			
			long fileStart = System.currentTimeMillis();
			System.out.print("Processing " + file.getName() + " ...");
			try {
				//fake database id
				Long dID = Long.valueOf(System.nanoTime());
				SBMLExtractor.extractStoreIndex(file,null,dID);
			} catch (FileNotFoundException e) {
				System.out.println("File " + file.getName() + "not found!");
				e.printStackTrace();
				System.exit(-1);
			} catch (XMLStreamException e) {
				System.out.println("File " + file.getName() + "caused XMLStreamException");
				e.printStackTrace();
				System.exit(-1);
			} catch (IOException e) {
				System.out.println("File " + file.getName() + "caused IOException!");
				e.printStackTrace();
				System.exit(-1);
			}
			System.out.println("done in " + (System.currentTimeMillis()-fileStart) + "ms");
		}
		System.out.println("all done at: "+ new Date() + " needed: " + (System.currentTimeMillis()-start)+ "ms");
	}

}
