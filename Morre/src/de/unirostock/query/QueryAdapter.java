package de.unirostock.query;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.Version;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexHits;

import de.unirostock.configuration.Config;
import de.unirostock.configuration.Property;
import de.unirostock.database.DBTraverser;
import de.unirostock.database.Manager;

public class QueryAdapter {

	private static Index<Node> nodeIndex =  null; //Manager.instance().getNodeIndex();
	//private static Index<Relationship> relationshipIndex = Manager.instance().getRelationshipIndex();
	//private static Index<Node> annotationIndex =  Manager.instance().getAnnotationIndex();
	private static Index<Node> annotationFull =  null; //Manager.instance().getAnnotationFullText();

	public static List<ResultSet> simpleQuery(String q) throws ParseException {
		if (StringUtils.isEmpty(q)) return new LinkedList<ResultSet>();
		if (nodeIndex==null) nodeIndex = Manager.instance().getNodeFullTextIndex();
		MultiFieldQueryParser parser = new MultiFieldQueryParser(Version.LUCENE_31, Config.nodeIndexFields, new StandardAnalyzer(Version.LUCENE_31));
		Query query = parser.parse(q);
		IndexHits<Node> hits = nodeIndex.query(query);

		if ((hits == null) || (hits.size() == 0)) {
			return new LinkedList<ResultSet>();
		}
		List<ResultSet> result = new LinkedList<ResultSet>();

		for (Iterator<Node> hitsIt = hits.iterator(); hitsIt.hasNext();) {		
			Node node = (Node) hitsIt.next();
			if (node.hasProperty(Property.General.NODE_TYPE) && StringUtils.equals((String) node.getProperty(Property.General.NODE_TYPE), Property.NodeType.MODEL)){
				Node docNode = DBTraverser.fromModelToDocument(node);
				Long databaseId = null;
				if (docNode.hasProperty(Property.General.DATABASEID)) databaseId = (Long)docNode.getProperty(Property.General.DATABASEID);
				ResultSet rs = new ResultSet(hits.currentScore(),(String)node.getProperty(Property.General.ID),(String)node.getProperty(Property.SBML.NAME), databaseId,null);
				result.add(rs);
			}
		}
		return result;
	}
	
	public static List<ResultSet> annotationQuery(String q, int bestN) throws ParseException {
		if (StringUtils.isEmpty(q)) return new LinkedList<ResultSet>();
		if (annotationFull==null) annotationFull = Manager.instance().getAnnotationFullTextIndex();
		q = "fulltext:("+q+")";
		QueryParser parser = new QueryParser(Version.LUCENE_31, q,
				new StandardAnalyzer(Version.LUCENE_31));
		Query query = parser.parse(q);
		IndexHits<Node> hits = annotationFull.query(query);

		if ((hits == null) || (hits.size() == 0)) {
			return new LinkedList<ResultSet>();
		}
		List<ResultSet> result = new LinkedList<ResultSet>();
		int n=0;
		
		for (Iterator<Node> hitsIt = hits.iterator(); hitsIt.hasNext();) {
			n++;
			Node annotationNode = (Node) hitsIt.next();
			result.addAll(DBTraverser.getResultsFromAnnotationToModel(annotationNode, hits.currentScore()));
			if (n>=bestN) break;
		}
		return result;
	}

	public static void main(String[] args) {

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-dbPath")) {
				Config.dbPath = args[++i];
			}
		}

		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from node index...");
			List<ResultSet> results = null;
			try {
				 results = simpleQuery(s);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("done");
			if ((results != null) && (results.size()>0)) {
				
				System.out.println("Found " + results.size() + " results");
				for (Iterator<ResultSet> iterator = results.iterator(); iterator.hasNext();) {
					ResultSet resultSet = (ResultSet) iterator.next();
					System.out.println("===============================================");
					System.out.println(resultSet.getModelScore());
					System.out.println(resultSet.getModelName());
					System.out.println(resultSet.getModelId());
					System.out.println(resultSet.getDatabaseId());					
			}
				
			} else System.out.print("No results!");
			System.out.println();
			//-> annotation index
			System.out.println("Retrieving from annotation index...");
			results = null;
			try {
				 results = annotationQuery(s,10);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("done");
			if ((results != null) && (results.size()>0)) {				
				System.out.println("Found " + results.size() + " results");
				for (Iterator<ResultSet> iterator = results.iterator(); iterator.hasNext();) {
					ResultSet resultSet = (ResultSet) iterator.next();
					System.out.println(resultSet.getModelName() + " " + resultSet.getModelId() + " " + resultSet.getModelScore());					
			}
				
			} else System.out.print("No results!");
			System.out.println();
			
		}
	}

}
