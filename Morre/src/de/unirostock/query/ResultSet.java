package de.unirostock.query;

public class ResultSet {
	private String modelName;
	private float score;
	private String modelID;
	private String explanation;
	private Long databaseID;
	
	
	public ResultSet(float score, String modelId, String modelName, Long databaseID, String explanation){
		this.modelName = modelName;
		this.score = score;
		this.modelID = modelId;
		this.explanation = explanation;
		this.databaseID = databaseID;
		
	}
	
//	public void set(String modelName, float score, String id, String explanation){
//		this.modelName = modelName;
//		this.score = score;
//		this.modelID = id;
//		this.explanation = explanation;
//	}
	
	
	public String getModelName() {
		return modelName;
	}
	
	public String getModelId() {
		return modelID;
	}

	public float getModelScore() {
		return score;
	}

	public String getSearchExplanation() {
		return explanation;
	}

	public Long getDatabaseId() {
		return databaseID;
	}


}
